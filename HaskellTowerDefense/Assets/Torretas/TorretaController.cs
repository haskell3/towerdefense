using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorretaController : MonoBehaviour
{
    public List<Enemigo> enemigos = new List<Enemigo>();
    public Enemigo CurrentEnemigo = null;
    public bool dispara = false;
    public TiposTorretas tipotorreta;
    public int atk;
    public float cooldown;
    public float area;
    // Start is called before the first frame update
    void Start()
    {
        this.cooldown = tipotorreta.cooldown;
        this.atk = tipotorreta.atk;
        this.gameObject.GetComponent<SpriteRenderer>().sprite = tipotorreta.sprite;
        this.gameObject.GetComponent<CircleCollider2D>().radius = tipotorreta.area;
        StartCoroutine(Disparar());
        
    }

    // Update is called once per frame
    void Update()
    {
        GetCurrentEnemigo();
        Rotar();

        if (enemigos.Count <= 0)
        {
            dispara = false;
        } else
        {
            dispara = true;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemigo"))
        {
            dispara = true;
            print("entra");
            Enemigo enemigo = collision.GetComponent<Enemigo>();
            enemigos.Add(enemigo);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemigo"))
        {
            Enemigo enemigo = collision.GetComponent<Enemigo>();
            if (enemigos.Contains(enemigo))
            {
                print("sale");
                enemigos.Remove(enemigo);
            }
        }
    }
    private void GetCurrentEnemigo()
    {
        if (enemigos.Count <= 0)
        {
            CurrentEnemigo = null;
            return;
        }
        CurrentEnemigo = enemigos[0];
    }

    private void Rotar()
    {
        if (CurrentEnemigo == null)
        {
            return;
        }
        Vector3 targetPos = CurrentEnemigo.transform.position -this.transform.position;
        float angulo = Vector3.SignedAngle(transform.up, targetPos, transform.forward);
        transform.Rotate(0f, 0f, angulo);
    }

    IEnumerator Disparar()
    {
        while (true)
        {
            
            if (dispara)
            {
                GameObject novabala = BalaPool.instance.GetPooledBala();
                if (novabala != null)
                {
                    novabala.GetComponent<Transform>().position = new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                    novabala.SetActive(true);
                    novabala.transform.localScale = new Vector2(tipotorreta.scalebala, tipotorreta.scalebala);
                    novabala.GetComponent<BalaController>().atk = this.atk;
                    novabala.gameObject.GetComponent<SpriteRenderer>().color = tipotorreta.colorbala;
                    novabala.GetComponent<Rigidbody2D>().velocity = this.transform.up.normalized * tipotorreta.velbala;
                }
                
            }
            yield return new WaitForSeconds(this.cooldown);

        }
    }
}
