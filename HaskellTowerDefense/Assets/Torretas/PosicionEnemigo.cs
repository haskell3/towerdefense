using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PosicionEnemigo : ScriptableObject
{
    public Vector2 posicion;
}
