using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Base : MonoBehaviour
{
    public int hp=1;
    private int madera;
    public maderaRecurso texto;
    public Image barraDeVida;
    public float vidaMaxima;
    public GameObject boom;
    public GameObject boomsoud;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (hp <= 0) 
        {
            boom.GetComponent<Animator>().enabled = true;
            boomsoud.GetComponent<AudioSource>().enabled = true;
            StartCoroutine(CanviEscena());
            
        }

        barraDeVida.fillAmount = hp / vidaMaxima;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemigo"))
        {
            this.hp -= collision.GetComponent<Enemigo>().atk;
        }
    }

    public void curar()
    {
        madera = PlayerPrefs.GetInt("Madera");
        Debug.Log(madera);
        if (madera >= 100)
        {
            madera -= 100;
            this.texto.GetComponent<TMPro.TextMeshProUGUI>().text = madera + "";
            texto.GetComponent<maderaRecurso>().recursos = madera;
            Debug.Log(madera);
            hp += 50;
            if (hp > 300)
            {
                hp = 300;
            }
        }
    }

    IEnumerator CanviEscena()
    {

        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("GameOver");
    }
}
