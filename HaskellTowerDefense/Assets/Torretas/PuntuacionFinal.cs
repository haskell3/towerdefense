using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntuacionFinal : MonoBehaviour
{
    int puntuacion;
    void Start()
    {
        puntuacion = PlayerPrefs.GetInt("Punts");
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Final Puntuation: " + puntuacion;
    }
}
