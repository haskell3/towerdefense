using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TiposTorretas : ScriptableObject
{
    public int atk;
    public float cooldown;
    public Sprite sprite;
    public float area;
    public float scalebala;
    public Color colorbala;
    public int velbala;
}
