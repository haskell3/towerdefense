using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaController : MonoBehaviour
{
    public int atk=0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.transform.position.x < -17 || this.gameObject.transform.position.x > 17)
        {
            gameObject.SetActive(false);
        }
        if (this.gameObject.transform.position.y < -9 || this.gameObject.transform.position.y > 9)
        {
            gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemigo"))
        {
            collision.GetComponent<Enemigo>().hp -= atk;
            gameObject.SetActive(false);
        }
    }
}
