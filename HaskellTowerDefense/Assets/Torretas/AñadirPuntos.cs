using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AñadirPuntos : MonoBehaviour
{
    int puntos;
    void Start()
    {
        puntos = 0;
    }

    public void Empezar()
    {
        StartCoroutine(morePoints());
    }

    IEnumerator morePoints()
    {
        while (true)
        {
            this.puntos += 100;
            PlayerPrefs.SetInt("Punts", puntos);
            this.GetComponent<TMPro.TextMeshProUGUI>().text = puntos + "";
            yield return new WaitForSeconds(1);
        }
    }
}
