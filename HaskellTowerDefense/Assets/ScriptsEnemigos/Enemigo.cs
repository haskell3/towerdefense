using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public int vel;
    public Color enfado;
    public int incremento;
    public int atk;
    public int hp;
    public int money;
    public Sprite[] sprites;
    public GameEventInt oro;
    [SerializeField]
    private Vector3[] waypoints;
    private int numWaypoint = 0;
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, 0);
        this.GetComponent<SpriteRenderer>().sprite = sprites[0];
    }
    void Update()
    {
        Vector3 vector = waypoints[numWaypoint] - this.transform.position;
        this.GetComponent<Rigidbody2D>().velocity = vector.normalized * vel;

        if (hp <= 0)
        {
            oro.Raise(money);
            Destroy(this.gameObject);
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            hp= 0;
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "WayPoint")
        {
            numWaypoint++;
            if (waypoints.Length == numWaypoint)
            {
                Destroy(this.gameObject);
            }else if(numWaypoint == 1 || numWaypoint == 5)
            {
                //Front
                this.GetComponent<SpriteRenderer>().sprite = sprites[2];
            }else if(numWaypoint == 3)
            {
                //Back
                this.GetComponent<SpriteRenderer>().sprite = sprites[1];
            }
            else
            {
                //Normal
                this.GetComponent<SpriteRenderer>().sprite = sprites[0];
            }
        }
    }
    public void enfadarse()
    {
        this.GetComponent<SpriteRenderer>().color = enfado;
        this.atk += incremento;
        this.vel += incremento;
    }
}
