using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public TiposEnemigos[] enemics;
    public GameObject prefab;
    public GameObject rey;
    private TiposEnemigos MainEnemy;
    private float sec = 5;
    public float dis;
    private int nivEne = 1;
    public GameEvent madera;
    public GameEvent puntos;

    public void Empezar()
    {
        StartCoroutine(newEnemy());
        StartCoroutine(pushUpDifficulty());
        StartCoroutine(lessSeconds());
        StartCoroutine(Boss());
        puntos.Raise();
        madera.Raise();
    }

    IEnumerator newEnemy() {
        while (true)
        {
            int numRan = Random.Range(0, 10);
            
            if(numRan <= 5)
            {
                if(nivEne == 1)
                {
                    MainEnemy = enemics[0];
                }else if(nivEne == 2)
                {
                    MainEnemy = enemics[1];
                }
                else
                {
                    MainEnemy = enemics[2];
                }
            }else if (numRan <= 8)
            {
                if (nivEne == 1)
                {
                    MainEnemy = enemics[3];
                }
                else if (nivEne == 2)
                {
                    MainEnemy = enemics[4];
                }
                else
                {
                    MainEnemy = enemics[5];
                }
            }
            else
            {
                if (nivEne == 1)
                {
                    MainEnemy = enemics[6];
                }
                else if (nivEne == 2)
                {
                    MainEnemy = enemics[7];
                }
                else
                {
                    MainEnemy = enemics[8];
                }
            }
            GameObject nuevo = Instantiate(prefab);
            nuevo.transform.localScale = new Vector3(MainEnemy.scale, MainEnemy.scale, 1);
            nuevo.GetComponent<Enemigo>().atk = MainEnemy.atk;
            nuevo.GetComponent<Enemigo>().hp = MainEnemy.hp;
            nuevo.GetComponent<Enemigo>().vel = MainEnemy.vel;
            nuevo.GetComponent<Enemigo>().money = MainEnemy.money;
            nuevo.GetComponent<Enemigo>().sprites = MainEnemy.sprite;
            if(numRan > 5 && numRan <= 8)
            {
                nuevo.transform.position = new Vector3(nuevo.transform.position.x, nuevo.transform.position.y + 0.25f, nuevo.transform.position.z);
            }
            yield return new WaitForSeconds(sec);
        }
    }
    IEnumerator pushUpDifficulty()
    {
        while (true)
        {
            yield return new WaitForSeconds(60);

            nivEne++;
            if (nivEne >= 3)
            {
                StopCoroutine(pushUpDifficulty());
            }

        }
    }
    IEnumerator lessSeconds()
    {
        while (true)
        {
            yield return new WaitForSeconds(10);
            sec = sec * dis;
        }
    }
    IEnumerator Boss()
    {
        while (true)
        {
            yield return new WaitForSeconds(300);
            GameObject nuevo = Instantiate(rey);
            nuevo.transform.localScale = new Vector3(enemics[9].scale, enemics[9].scale, 1);
            nuevo.GetComponent<Rey>().atk = enemics[9].atk;
            nuevo.GetComponent<Rey>().hp = enemics[9].hp;
            nuevo.GetComponent<Rey>().vel = enemics[9].vel;
            nuevo.GetComponent<Rey>().money = enemics[9].money;
            nuevo.GetComponent<Rey>().sprites = enemics[9].sprite;
            nuevo.transform.position = new Vector3(nuevo.transform.position.x, nuevo.transform.position.y + 0.25f, nuevo.transform.position.z);
        }
    }
}
