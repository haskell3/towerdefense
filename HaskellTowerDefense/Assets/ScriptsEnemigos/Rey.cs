using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rey : MonoBehaviour
{
    public GameEvent muerto;
    public int vel;
    public int atk;
    public int hp;
    public int money;
    public Sprite[] sprites;
    [SerializeField]
    private Vector3[] waypoints;
    private int numWaypoint = 0;
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, 0);
        this.GetComponent<SpriteRenderer>().sprite = sprites[0];
    }
    void Update()
    {
        Vector3 vector = waypoints[numWaypoint] - this.transform.position;
        this.GetComponent<Rigidbody2D>().velocity = vector.normalized * vel;

        //Esto era para las pruebas despues esto del hp menor 0 seria cuando las torres de Gisela maten.
        /*if(hp <= 0)
        {
            muerto.Raise();
            Destroy(this.gameObject);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            hp = 0;
        }*/
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "WayPoint")
        {
            numWaypoint++;
            if (waypoints.Length == numWaypoint)
            {
                Destroy(this.gameObject);
                //Aqui falta Da�o a la torre pero ni esta programada la torre asique XD.
            }else if(numWaypoint == 1 || numWaypoint == 5)
            {
                //Front
                this.GetComponent<SpriteRenderer>().sprite = sprites[2];
            }else if(numWaypoint == 3)
            {
                //Back
                this.GetComponent<SpriteRenderer>().sprite = sprites[1];
            }
            else
            {
                //Normal
                this.GetComponent<SpriteRenderer>().sprite = sprites[0];
            }
        }
    }
}
