using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TiposEnemigos : ScriptableObject
{
    public int atk;
    public int hp;
    public int vel;
    public Sprite[] sprite;
    public float scale;
    public int money;
}
