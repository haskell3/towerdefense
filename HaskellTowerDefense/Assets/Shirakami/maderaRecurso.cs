using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class maderaRecurso : MonoBehaviour
{
    public int recursos;
    // Start is called before the first frame update
    void Start()
    {
        recursos = 0;
    }

    public void Empezar()
    {
        StartCoroutine(morePoints());
    }

    IEnumerator morePoints()
    {
        while (true)
        {
            this.recursos += 1;
            PlayerPrefs.SetInt("Madera", recursos);
            this.GetComponent<TMPro.TextMeshProUGUI>().text = recursos + "";
            yield return new WaitForSeconds(1);
        }
    }
}
