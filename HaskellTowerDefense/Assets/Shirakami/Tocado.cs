using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tocado : MonoBehaviour
{
    public TiposTorretas[] tiposTorretas;
    public TiposTorretas tipotorreta;
    public GameObject torreta;
    public oroRecurso texto;
    private int oro;
    // Start is called before the first frame update
    private void OnMouseDown()
    {
        oro = PlayerPrefs.GetInt("Oro");
        Debug.Log(tipotorreta);

        if (tipotorreta != null)
        {
            if (oro >= 150)
            {
                oro -= 150;
                this.texto.GetComponent<TMPro.TextMeshProUGUI>().text = oro + "";
                texto.GetComponent<oroRecurso>().recursos = oro;
                GameObject nueva = Instantiate(torreta);
                nueva.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);  
                nueva.GetComponent<TorretaController>().tipotorreta = this.tipotorreta;
                this.gameObject.SetActive(false);
            }
           
        }
       
    }   
    public void Garga()
    {
        tipotorreta = tiposTorretas[0];
    }

    public void Toxci()
    {
        tipotorreta = tiposTorretas[1];
    }

    public void Articuno()
    {
        tipotorreta = tiposTorretas[2];
    }


}
