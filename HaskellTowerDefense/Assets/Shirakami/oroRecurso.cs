using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class oroRecurso : MonoBehaviour
{
    public int recursos = 100;
    void Start()
    {
        PlayerPrefs.SetInt("Oro", recursos);
    }
    public void obtener(int oro)
    {
        recursos += oro;
        PlayerPrefs.SetInt("Oro", recursos);
        this.GetComponent<TMPro.TextMeshProUGUI>().text = recursos + "";
    }
}
